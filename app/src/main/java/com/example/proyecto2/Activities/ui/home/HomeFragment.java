package com.example.proyecto2.Activities.ui.home;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.proyecto2.Activities.MenuNavegacion;
import com.example.proyecto2.Api.Api;
import com.example.proyecto2.R;
import com.example.proyecto2.Servicios.servicioPeticion;
import com.example.proyecto2.ViewModels.Alertas_usuario;
import com.example.proyecto2.ViewModels.Peticion_crear_alerta;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });
        return root;
    }

    private String Usuariotxt;
    private int idusuario=0;
    final ArrayList<Integer> listaalertas=new ArrayList<>();
    final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
    final ArrayList<String> fecha_creacion=new ArrayList<>();
    final ArrayList<String> fecha_actualizacion=new ArrayList<>();
    private ListView listaAlertas;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Button Crearalerta;
        Crearalerta=view.findViewById(R.id.Crear_alerta);
        listaAlertas=view.findViewById(R.id.listaMisalertas);
        obtenerid();
        LlenarListview();

        listaAlertas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });
        Crearalerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listaAlertas.setAdapter(null);
                CrearAlerta();
                LlenarListview();
            }
        });
    }
    public void obtenerid(){
        SharedPreferences preferencias=getActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Usuariotxt=preferencias.getString("Idusuario","");
        idusuario= Integer.parseInt(Usuariotxt);
    }

    public void LlenarListview(){
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<com.example.proyecto2.ViewModels.Alertas_usuario> AlertasCall= service .Alertasusuario(idusuario);
        AlertasCall.enqueue(new Callback<Alertas_usuario>() {
            @Override
            public void onResponse(Call<com.example.proyecto2.ViewModels.Alertas_usuario> call, Response<com.example.proyecto2.ViewModels.Alertas_usuario> response) {
                final com.example.proyecto2.ViewModels.Alertas_usuario peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    listaalertas.clear();
                    listaidUsuarios.clear();
                    fecha_creacion.clear();
                    fecha_actualizacion.clear();
                    for(int i=0;i<peticion.alertas.size();i++){
                        listaalertas.add(peticion.alertas.get(i).id);
                        listaidUsuarios.add(peticion.alertas.get(i).usuarioId);
                        fecha_creacion.add(peticion.alertas.get(i).created_at);
                        fecha_actualizacion.add(peticion.alertas.get(i).updated_at);
                    }
                    ArrayAdapter adapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,listaalertas);
                    listaAlertas.setAdapter(adapter);
                }else{
                    Toast.makeText(getContext(),peticion.detalle,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<com.example.proyecto2.ViewModels.Alertas_usuario> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void CrearAlerta() {

        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Peticion_crear_alerta> AlertasCall= service .Crear_alerta(idusuario);
        AlertasCall.enqueue(new Callback<Peticion_crear_alerta>() {
            @Override
            public void onResponse(Call<Peticion_crear_alerta> call, Response<Peticion_crear_alerta> response) {
                Peticion_crear_alerta peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    Toast.makeText(getContext(),"Alerta creada correctamente",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(getContext(),peticion.detalle,Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Peticion_crear_alerta> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void obtenerinformacion(Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion);
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}