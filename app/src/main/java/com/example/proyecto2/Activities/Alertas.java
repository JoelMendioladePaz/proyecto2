package com.example.proyecto2.Activities;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.proyecto2.Api.Api;
import com.example.proyecto2.R;
import com.example.proyecto2.Servicios.servicioPeticion;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Alertas#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Alertas extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Alertas() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Alertas.
     */
    // TODO: Rename and change types and number of parameters
    public static Alertas newInstance(String param1, String param2) {
        Alertas fragment = new Alertas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_alertas, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ListView lista;
        final ArrayList<Integer> listaalertas=new ArrayList<>();
        final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
        final ArrayList<String> fecha_creacion=new ArrayList<>();
        final ArrayList<String> fecha_actualizacion=new ArrayList<>();
        lista=view.findViewById(R.id.Listaalertas);
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<com.example.proyecto2.ViewModels.Alertas> AlertasCall= service .Alertas();
        AlertasCall.enqueue(new Callback<com.example.proyecto2.ViewModels.Alertas>() {
            @Override
            public void onResponse(Call<com.example.proyecto2.ViewModels.Alertas> call, Response<com.example.proyecto2.ViewModels.Alertas> response) {
                final com.example.proyecto2.ViewModels.Alertas peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.alertas != null){
                    for(int i=0;i<peticion.alertas.size();i++){
                        listaalertas.add(peticion.alertas.get(i).id);
                        listaidUsuarios.add(peticion.alertas.get(i).usuarioId);
                        fecha_creacion.add(peticion.alertas.get(i).created_at);
                        fecha_actualizacion.add(peticion.alertas.get(i).updated_at);
                    }
                    ArrayAdapter adapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,listaalertas);
                    lista.setAdapter(adapter);
                }else{
                    Toast.makeText(getContext(),"ocurrio un error",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<com.example.proyecto2.ViewModels.Alertas> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });
    }

    private void obtenerinformacion(Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion);
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}