package com.example.proyecto2.Activities.ui.gallery;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.proyecto2.Api.Api;
import com.example.proyecto2.R;
import com.example.proyecto2.Servicios.servicioPeticion;
import com.example.proyecto2.ViewModels.Visualizaciones;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        galleryViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        return root;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ListView lista;
        final ArrayList<Integer> listaalertas=new ArrayList<>();
        final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
        final ArrayList<Integer> listaidalerta=new ArrayList<>();
        final ArrayList<String> fecha_creacion=new ArrayList<>();
        final ArrayList<String> fecha_actualizacion=new ArrayList<>();
        lista=view.findViewById(R.id.ListaVisualizaciones);
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Visualizaciones> visualizacionesCall= service .Visualizaciones();
        visualizacionesCall.enqueue(new Callback<Visualizaciones>() {
            @Override
            public void onResponse(Call<com.example.proyecto2.ViewModels.Visualizaciones> call, Response<Visualizaciones> response) {
                final com.example.proyecto2.ViewModels.Visualizaciones peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.visualizaciones != null){
                    for(int i=0;i<peticion.visualizaciones.size();i++){
                        listaalertas.add(peticion.visualizaciones.get(i).id);
                        listaidUsuarios.add(peticion.visualizaciones.get(i).usuarioId);
                        listaidalerta.add(peticion.visualizaciones.get(i).alertaId);
                        fecha_creacion.add(peticion.visualizaciones.get(i).created_at);
                        fecha_actualizacion.add(peticion.visualizaciones.get(i).updated_at);
                    }
                    ArrayAdapter adapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,listaalertas);
                    lista.setAdapter(adapter);
                }else{
                    Toast.makeText(getContext(),"ocurrio un error",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<com.example.proyecto2.ViewModels.Visualizaciones> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaidalerta.get(position),listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });
    }

    private void obtenerinformacion(Integer idalerta,Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion+"\n" +
                "id alerta: "+idalerta);
        builder.setPositiveButton("Aceptar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}