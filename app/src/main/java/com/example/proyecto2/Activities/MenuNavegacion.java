package com.example.proyecto2.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.Toast;

import com.example.proyecto2.Api.Api;
import com.example.proyecto2.R;
import com.example.proyecto2.Servicios.servicioPeticion;
import com.example.proyecto2.ViewModels.Peticion_crear_alerta;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuNavegacion extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    String APITOKEN="";
    String IDUSUARIO="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_navegacion);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setImageDrawable(Drawable.createFromPath("@drawable/ic_baseline_mood_24"));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Cerrando sesion....", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Cerrarsesion();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,R.id.alertas)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_navegacion, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    public void Cerrarsesion(){
        guardarPreferencias();
        startActivity(new Intent(this,Login.class));
    }
    private void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString("Token",APITOKEN);
        editor.putString("Idusuario",IDUSUARIO);
        editor.commit();
    }
    private String Usuariotxt;
    private int idusuario=0;
    public void CrearAlerta(int idalerta) {
        obtenerid();
        servicioPeticion service = Api.getApi(this).create(servicioPeticion.class);
        Call<Peticion_crear_alerta> visualizacionCall= service .Crear_visualizacion(idusuario,idalerta);
        visualizacionCall.enqueue(new Callback<Peticion_crear_alerta>() {
            @Override
            public void onResponse(Call<Peticion_crear_alerta> call, Response<Peticion_crear_alerta> response) {
                Peticion_crear_alerta peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(MenuNavegacion.this,"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    Toast.makeText(MenuNavegacion.this,"Alerta creada correctamente",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(MenuNavegacion.this,peticion.detalle,Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Peticion_crear_alerta> call, Throwable t) {
                Toast.makeText(MenuNavegacion.this,"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void obtenerid(){
        SharedPreferences preferencias=getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Usuariotxt=preferencias.getString("Idusuario","");
        idusuario= Integer.parseInt(Usuariotxt);
    }
}