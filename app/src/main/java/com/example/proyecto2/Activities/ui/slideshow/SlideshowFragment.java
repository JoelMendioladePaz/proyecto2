package com.example.proyecto2.Activities.ui.slideshow;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.proyecto2.Api.Api;
import com.example.proyecto2.R;
import com.example.proyecto2.Servicios.servicioPeticion;
import com.example.proyecto2.ViewModels.Peticion_crear_alerta;
import com.example.proyecto2.ViewModels.Visualizaciones_usuario;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        //final TextView textView = root.findViewById(R.id.text_slideshow);
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
          //      textView.setText(s);
            }
        });
        return root;
    }
    private String Usuariotxt;
    private int idusuario=0;
    final ArrayList<Integer> listaalertas=new ArrayList<>();
    final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
    final ArrayList<Integer> listaidalerta=new ArrayList<>();
    final ArrayList<String> fecha_creacion=new ArrayList<>();
    final ArrayList<String> fecha_actualizacion=new ArrayList<>();
    private ListView listaVisualizaciones;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaVisualizaciones =view.findViewById(R.id.listaMisvisualizaciones);
        obtenerid();
        LlenarListview();
        listaVisualizaciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaalertas.get(position),listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });

    }
    public void obtenerid(){
        SharedPreferences preferencias=getActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Usuariotxt=preferencias.getString("Idusuario","");
        idusuario= Integer.parseInt(Usuariotxt);
    }

    public void LlenarListview(){
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Visualizaciones_usuario> AlertasCall= service .Visualizacionesusuario(idusuario);
        AlertasCall.enqueue(new Callback<Visualizaciones_usuario>() {
            @Override
            public void onResponse(Call<com.example.proyecto2.ViewModels.Visualizaciones_usuario> call, Response<Visualizaciones_usuario> response) {
                final com.example.proyecto2.ViewModels.Visualizaciones_usuario peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    listaalertas.clear();
                    listaidUsuarios.clear();
                    listaidalerta.clear();
                    fecha_creacion.clear();
                    fecha_actualizacion.clear();
                    for(int i=0;i<peticion.visualizaciones.size();i++){
                        listaalertas.add(peticion.visualizaciones.get(i).id);
                        listaidUsuarios.add(peticion.visualizaciones.get(i).usuarioId);
                        listaidalerta.add(peticion.visualizaciones.get(i).alertaId);
                        fecha_creacion.add(peticion.visualizaciones.get(i).created_at);
                        fecha_actualizacion.add(peticion.visualizaciones.get(i).updated_at);
                    }
                    ArrayAdapter adapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,listaalertas);
                    listaVisualizaciones.setAdapter(adapter);
                }else{
                    Toast.makeText(getContext(),peticion.detalle,Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<com.example.proyecto2.ViewModels.Visualizaciones_usuario> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void obtenerinformacion(Integer idalerta,Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion +"\n" +
                "id Alerta: "+ idalerta);
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}