package com.example.proyecto2.Servicios;



import com.example.proyecto2.ViewModels.Alertas;
import com.example.proyecto2.ViewModels.Alertas_usuario;
import com.example.proyecto2.ViewModels.Peticion_Login;
import com.example.proyecto2.ViewModels.Peticion_crear_alerta;
import com.example.proyecto2.ViewModels.Registro_Usuario;
import com.example.proyecto2.ViewModels.Visualizaciones;
import com.example.proyecto2.ViewModels.Visualizaciones_usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface servicioPeticion {


    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario>registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> login(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<Peticion_crear_alerta> Crear_alerta(@Field("usuarioId") int id);

    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<Alertas_usuario> Alertasusuario(@Field("usuarioId") int id);

    @POST("api/alertas")
    Call<Alertas> Alertas();

    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<Peticion_crear_alerta> Crear_visualizacion(@Field("usuarioId") int id,@Field("alertaId") int aletaid);

    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<Visualizaciones_usuario> Visualizacionesusuario(@Field("usuarioId") int id);

    @POST("api/visualizaciones")
    Call<Visualizaciones> Visualizaciones();
}
