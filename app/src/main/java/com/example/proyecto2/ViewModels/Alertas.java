package com.example.proyecto2.ViewModels;

import java.util.List;

public class Alertas {
    public List<Alertas_detalles> alertas;

    public List<Alertas_detalles> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Alertas_detalles> alertas) {
        this.alertas = alertas;
    }
}
