package com.example.proyecto2.ViewModels;

import java.util.List;

public class Visualizaciones {
    public List<Visualizaciones_detalles> visualizaciones;

    public List<Visualizaciones_detalles> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Visualizaciones_detalles> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}
