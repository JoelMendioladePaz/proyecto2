package com.example.proyecto2.ViewModels;

import java.util.List;

public class Visualizaciones_usuario {
    public String estado;
    public String detalle;
    public List<Visualizaciones_detalles> visualizaciones;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public List<Visualizaciones_detalles> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Visualizaciones_detalles> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}
