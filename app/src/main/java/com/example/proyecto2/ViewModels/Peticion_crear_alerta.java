package com.example.proyecto2.ViewModels;

public class Peticion_crear_alerta {
    public String estado;
    public String detalle;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
